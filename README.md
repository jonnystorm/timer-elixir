# TimerEx

A tiny functional timer library for Elixir.

## Usage

```elixir
timer = Timer.new

started_timer = Timer.start(timer)

stopped_timer = Timer.stop(started_timer)

elapsed_ms = Timer.elapsed(timer)

elapsed_s = Timer.elapsed(timer, precision: :sec)

elapsed_min = Timer.elapsed(timer, precision: :min, round: :up)
```

There are conveniences for handling timeouts, as well:

```elixir
iex(1)> timer = Timer.new(timeout: 2000)
iex(2)> Timer.timed_out?(timer)
false
iex(3)> started_timer = Timer.start(timer)
iex(4)> Timer.timed_out?(timer)
false
iex(5)> Timer.timed_out?(timer)
true
iex(6)> stopped_timer = Timer.stop(started_timer)
iex(7)> Timer.timed_out?(stopped_timer)
true
```

## Installation

Add `timer_ex` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [ { :timer_ex,
      git: "https://gitlab.com/jonnystorm/timer-elixir.git"
    },
  ]
end
```

