defmodule TimerTest do
  use ExUnit.Case
  doctest Timer

  test "determines that a stopped timer is stopped" do
    assert Timer.is_started(%Timer{}) == false
  end

  test "determines that a started timer is started" do
    assert Timer.is_started(%Timer{start: 0}) == true
  end

  test "starts a stopped timer" do
    timer = Timer.new

    assert Timer.is_started(timer) == false

    timer = Timer.start(timer)

    assert Timer.is_started(timer) == true
  end

  test "stops a started timer" do
    timer = Timer.start(Timer.new)

    assert Timer.is_started(timer) == true

    timer = Timer.stop(timer)

    assert Timer.is_started(timer) == false
  end

  test "starting a started timer does nothing" do
    timer = Timer.start(Timer.new)

    assert Timer.is_started(timer) == true

    timer = Timer.start(timer)

    assert Timer.is_started(timer) == true
  end

  test "stopping a stopped timer does nothing" do
    timer = Timer.new

    assert Timer.is_started(timer) == false

    timer = Timer.stop(timer)

    assert Timer.is_started(timer) == false
  end

  test "calculates time elapsed from start to stop" do
    timer = Timer.start(Timer.new)

    :timer.sleep 1000

    timer = Timer.stop(timer)

    assert Timer.elapsed(timer, precision: :sec) >= 1
  end

  test "calculates time elapsed while running" do
    timer = Timer.start(Timer.new)

    :timer.sleep 500

    assert Timer.elapsed(timer, precision: :sec) == 0

    :timer.sleep 500

    assert Timer.elapsed(timer, precision: :sec) == 1
  end

  test "accumulates time elapsed across multiple runs" do
    timer = Timer.start(Timer.new)

    :timer.sleep 500

    timer = Timer.stop(timer)

    :timer.sleep 1000

    timer = Timer.start(timer)

    :timer.sleep 500

    assert Timer.elapsed(timer, precision: :sec) == 1
  end
end
