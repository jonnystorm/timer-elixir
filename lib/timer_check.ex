defmodule TimerCheck do
  @moduledoc false

  import Timer

  # Invoke all of the timer API functions under various
  # circumstances to trigger dialyzer's success typing.

  def scenario1 do
    timer = new()
    started = start(timer)
    true = is_started(started)
    stopped = stop(started)
    false = is_started(stopped)
    _ = elapsed(stopped)
    _ = elapsed(stopped, precision: :sec)
    _ = elapsed(stopped, precision: :sec, round: :up)
  end

  def scenario2 do
    timer = new(timeout: 10)
    100 = timeout(timer)
    false = timed_out?(timer)
    started = start(timer)
    true = is_started(started)
    _remaining = Timer.remaining(started)
    :timer.sleep(10)
    true = timed_out?(started)
    stopped = stop(started)
    false = is_started(stopped)
    true = timed_out?(stopped)
  end
end
