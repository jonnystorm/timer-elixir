# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

defmodule Timer do
  require Logger

  defstruct(
    start: nil,
    elapsed: 0,
    timeout: :infinity
  )

  @type start   :: integer | nil
  @type elapsed :: non_neg_integer

  @type t
    :: %Timer{
      start:   start,
      elapsed: elapsed,
      timeout: timeout,
    }

  @type timer
  :: t
   | %{start:   start,
       elapsed: elapsed,
       timeout: timeout,
     }

  defp system_time,
    do: System.monotonic_time(:millisecond)

  defp total_elapsed(timer) do
    systime = system_time()
    start   = timer.start || systime

    systime - start + timer.elapsed
  end

  @spec start(timer)
    :: timer
  def start(timer) do
    %{timer |
      start: timer.start || system_time(),
    }
  end

  @spec stop(timer)
    :: timer
  def stop(timer) do
    %{timer |
      start:   nil,
      elapsed: total_elapsed(timer),
    }
  end

  @type opts :: Keyword.t

  @spec new(opts)
    :: t
  def new(opts \\ [])

  def new(opts)
      when is_list(opts)
  do
    timeout =
      Keyword.get(opts, :timeout, :infinity)

    if (is_integer(timeout) and timeout >= 0)
       or timeout == :infinity,
      do: %Timer{timeout: timeout},
    else: %Timer{}
  end

  @spec is_started(timer)
    :: boolean
  def is_started(%{start: nil}), do: false
  def is_started(%{start:   _}), do: true
  def is_started(_),
    do: raise "Provided argument is not a timer"

  defp format_elapsed(elapsed, precision, rounding) do
    fun =
      case rounding do
        :up      -> &:math.ceil/1
        :down    -> &trunc/1
        :nearest -> &round/1
      end

    case precision do
      :msec         -> elapsed
      :sec          -> fun.(elapsed/1000)
      :min          -> fun.(elapsed/(1000*60))
      :hour         -> fun.(elapsed/(1000*60*60))
      :quarter_hour -> fun.(elapsed/(1000*60*15)) * 0.25
    end
  end

  @type precision
    :: :msec
     | :sec
     | :min
     | :hour
     | :quarter_hour

  @spec elapsed(timer, opts)
    :: elapsed
     | :error
  def elapsed(timer, opts \\ [])

  def elapsed(
    %{start: _, elapsed: _} = timer,
    opts
  ) do
    precision =
      Keyword.get(opts, :precision, :msec)

    rounding =
      Keyword.get(opts, :round, :down)

    total = total_elapsed(timer)

    cond do
      total >= 0 ->
        format_elapsed(total, precision, rounding)

      total < 0 ->
        _ = Logger.warn("Timer appears to have been started on a different BEAM instance: total elapsed time is negative")

        :error
    end
  end

  @spec timeout(timer)
    :: timeout
  def timeout(timer)

  def timeout(%{timeout: timeout})
      when (is_integer(timeout) and timeout >= 0)
        or timeout == :infinity,
    do: timeout

  @spec remaining(timer)
    :: timeout
  def remaining(timer)

  def remaining(
    %{start: start,
      elapsed: elapsed,
      timeout: timeout,
    } = timer
  )   when (is_integer(start) or is_nil(start))
       and is_integer(elapsed) and elapsed >= 0
       and is_integer(timeout) and timeout >= 0,
    do: timeout - Timer.elapsed(timer)

  def remaining(_),
    do: :infinity

  @doc """
  Check if a timer has timed out.

  ## Examples

      Timer.timed_out?(timer)
      false

      Timer.timed_out?(timer)
      true

      Timer.timed_out?(timer)
      :error

      iex(1)> timer = Timer.new
      %Timer{start: nil, elapsed: 0, timeout: :infinity}
      iex(2)> Timer.timed_out?(timer)
      false

      iex(3)> timer = Timer.new(timeout: :infinity)
      %Timer{start: nil, elapsed: 0, timeout: :infinity}
      iex(4)> Timer.timed_out?(timer)
      false

      iex(5)> timer = Timer.new(timeout: 0)
      %Timer{start: nil, elapsed: 0, timeout: 0}
      iex(6)> Timer.timed_out?(timer)
      true

      iex(7)> timer = Timer.new(timeout: 5000)
      %Timer{start: nil, elapsed: 0, timeout: 5000}
      iex(8)> Timer.timed_out?(timer)
      false

      iex(9)> timer =
      ...(9)>   %Timer{start: nil, elapsed: 6003, timeout: 5000}
      iex(10)> Timer.timed_out?(timer)
      true
  """
  @spec timed_out?(timer)
    :: boolean
  def timed_out?(timer)

  def timed_out?(
    %{start:   start,
      elapsed: elapsed,
      timeout: timeout,
    } = timer
  )   when (is_integer(start) or is_nil(start))
       and is_integer(elapsed) and elapsed >= 0
       and is_integer(timeout) and timeout >= 0
  do
    with elapsed when is_integer(elapsed) <-
           Timer.elapsed(timer),
      do: elapsed >= timeout
  end

  def timed_out?(_),
    do: false
end
